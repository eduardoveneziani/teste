CREATE TYPE calls_state AS ENUM('begin', 'continue','end');

CREATE TABLE IF NOT EXISTS public.calls
(
    type calls_state,
    id_call integer,
    operator character varying,
    target character varying,
    source character varying,
    destination character varying,
    "timestamp" timestamp without time zone,
    cell character varying
)


BEGIN;

INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('begin', 1, 'vivo', '111111', '111111', '100001', '2021-12-30 22:00:00', '1-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('continue', 1, 'vivo', '111111', '111111', '100001', '2021-12-30 22:01:00', '1-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('end', 1, 'vivo', '111111', '111111', '100001', '2021-12-30 22:02:00', '1-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('begin', 2, 'claro', '222222', '222222', '200002', '2021-12-30 22:03:00', '2-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('continue', 2, 'claro', '222222', '222222', '200002', '2021-12-30 22:04:00', '2-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('end', 2, 'claro', '222222', '222222', '200002', '2021-12-30 22:05:00', '2-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('begin', 3, 'tim', '333333', '333333', '300003', '2021-12-30 22:06:00', '3-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('continue', 3, 'tim', '333333', '333333', '300003', '2021-12-30 22:07:00', '3-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('end', 3, 'tim', '333333', '333333', '300003', '2021-12-30 22:08:00', '3-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('begin', 4, 'oi', '444444', '444444', '400004', '2021-12-30 22:09:00', '4-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('continue', 4, 'oi', '444444', '444444', '400004', '2021-12-30 22:10:00', '4-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('end', 4, 'oi', '444444', '444444', '400004', '2021-12-30 22:11:00', '4-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('begin', 5, 'vivo', '555555', '555555', '500005', '2021-12-30 22:12:00', '5-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('continue', 5, 'vivo', '555555', '555555', '500005', '2021-12-30 22:13:00', '5-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('end', 5, 'vivo', '555555', '555555', '500005', '2021-12-30 22:14:00', '5-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('begin', 6, 'tim', '666666', '666666', '600006', '2021-12-30 22:15:00', '6-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('continue', 6, 'tim', '666666', '666666', '600006', '2021-12-30 22:16:00', '6-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('end', 6, 'tim', '666666', '666666', '600006', '2021-12-30 22:17:00', '6-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('begin', 7, 'claro', '777777', '777777', '700007', '2021-12-30 22:18:00', '7-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('continue', 7, 'claro', '777777', '777777', '700007', '2021-12-30 22:19:00', '7-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('end', 7, 'claro', '777777', '777777', '700007', '2021-12-30 22:20:00', '7-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('begin', 8, 'oi', '888888', '888888', '800008', '2021-12-30 22:21:00', '8-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('continue', 8, 'oi', '888888', '888888', '800008', '2021-12-30 22:22:00', '8-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('end', 8, 'oi', '888888', '888888', '800008', '2021-12-30 22:23:00', '8-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('begin', 9, 'vivo', '999999', '999999', '900009', '2021-12-30 22:24:00', '9-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('continue', 9, 'vivo', '999999', '999999', '900009', '2021-12-30 22:25:00', '9-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('end', 9, 'vivo', '999999', '999999', '900009', '2021-12-30 22:26:00', '9-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('begin', 10, 'tim', '101010', '101010', '100010', '2021-12-30 22:27:00', '10-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('continue', 10, 'tim', '101010', '101010', '100010', '2021-12-30 22:28:00', '10-ONONONON');
INSERT INTO public.calls(type, id_call, operator, target, source, destination, "timestamp", cell) VALUES ('end', 10, 'tim', '101010', '101010', '100010', '2021-12-30 22:29:00', '10-ONONONON');

END;

DROP FUNCTION f_list_calls(text,text,text,text,timestamp without time zone,timestamp without time zone,text);

CREATE OR REPLACE FUNCTION f_list_calls(
	poperator text default null, 
	ptarget text default null, 
	psource text default null, 
	pdestination text default null, 
	pdt_start timestamp without time zone default null, 
	pdt_end timestamp without time zone default null, 
	cell text default null
--) RETURNS refcursor AS $$
) RETURNS text AS $$

  DECLARE
  
    ref refcursor;                                                     -- Declare a cursor variable
	command text :='';
	ctrl boolean := true;
	
  BEGIN
  
  	IF poperator IS NULL
		AND ptarget IS NULL
		AND psource IS NULL
		AND pdestination IS NULL
		AND pdt_start IS NULL
		AND pdt_end IS NULL
		AND cell IS NULL
	THEN
	
		command := 'select * from calls limit 10';
		
	ELSE
		
		command := 'select * from calls where';
	
	  	IF poperator IS NOT NULL THEN
			IF ctrl THEN
				command := command || ' operator ilike (''%' || poperator || '%'')';
				ctrl := false;
			ELSE
						command := command || ' and operator ilike (''%' || poperator || '%'')';
			END IF;	
		END IF;
		
		IF ptarget IS NOT NULL THEN
			IF ctrl THEN
				command := command || ' target = ''' || ptarget || '''' ;
				ctrl := false;
					ELSE
					command := command || ' and target = ''' || ptarget || '''' ;
			END IF;	
		END IF;
		
		IF psource IS NOT NULL THEN
			IF ctrl THEN
				command := command || ' source ilike (''%' || psource || '%'')';
				ctrl := false;
					ELSE
					command := command || ' and source ilike (''%' || psource || '%'')';
			END IF;	
		END IF;
		
		IF pdestination IS NOT NULL THEN
			IF ctrl THEN
				command := command || ' destination ilike (''%' || pdestination || '%'')';
				ctrl := false;
					ELSE
					command := command || ' and destination ilike (''%' || pdestination || '%'')';
			END IF;	
		END IF;
		
		IF pdt_start IS NOT NULL THEN
			IF ctrl THEN
				command := command || ' timestamp = ''' || pdt_start || '''' ;
				ctrl := false;
					ELSE
					command := command || ' and timestamp = ''' || pdt_start || '''' ;
			END IF;	
		END IF;
		
		IF pdt_end IS NOT NULL THEN
			IF ctrl THEN
				command := command || ' timestamp = ''' || pdt_end || '''' ;
				ctrl := false;
					ELSE
					command := command || ' and timestamp = ''' || pdt_end || '''' ;
			END IF;	
		END IF;
		
		IF cell IS NOT NULL THEN
			IF ctrl THEN
				command := command || ' cell ilike (''%' || cell || '%'')';
				ctrl := false;
					ELSE
					command := command || ' and cell ilike (''%' || cell || '%'')';
			END IF;	
		END IF;
	
	execute(command);

	--RETURN command;
	
	END IF;

    RETURN command;
  
--    OPEN ref FOR SELECT * FROM calls;   -- Open a cursor
--    RETURN ref;                                                       -- Return the cursor to the caller
  END;
$$ LANGUAGE plpgsql;

--select f_list_calls();
select f_list_calls(poperator := 'aaa',psource := 'bbbb', pdt_end := '2021-12-31 12:21:00');

